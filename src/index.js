import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import { Rehydrated } from 'aws-appsync-react';
import { AppContainer } from 'react-hot-loader';
import { MuiThemeProvider } from '@material-ui/core/styles';

import { theme } from './theme';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import App from 'components/App';
import { client } from './apollo';


const render = Component => {
    ReactDOM.render(
        <AppContainer>
            <MuiThemeProvider theme={ theme }>
                <ApolloProvider client={ client }>
                    <Rehydrated>
                        <Component />
                    </Rehydrated>
                </ApolloProvider>
            </MuiThemeProvider>
        </AppContainer>,
        document.getElementById('root')
    );
}
registerServiceWorker();

render(App)
if (module.hot) {
    module.hot.accept('components/App', () => { render(App) });
}
