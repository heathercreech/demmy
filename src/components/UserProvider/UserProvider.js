import React, { Component, Fragment } from 'react';
import { Auth, Cache } from 'aws-amplify';
import { navigate } from '@reach/router';
import { client } from 'apollo/client';
import gql from 'graphql-tag';

const Wrapper = ({ children }) => <Fragment>{ children }</Fragment>;

const Context = React.createContext();
export const UserConsumer = Context.Consumer;

export class UserProvider extends Component {

    state = {
        user: '',
        id: '',
        image: '',
        userSet: false
    }

    handleChange = e => this.setState({ user: e.target.value })
    handleClick = () => this.state.user ? this.setState({ userSet: true }) : null
    signIn = (googleUser) => {
        const  { id_token, expires_at } = googleUser.getAuthResponse();
        const profile = googleUser.getBasicProfile();
        const user = {
            email: profile.getEmail(),
            name: profile.getName(),
            image: profile.getImageUrl()
        };
        
        return Auth.federatedSignIn('google', { token: id_token, expires_at }, user).then(() => {
            this.setState({ user: user.name, userSet: true, image: user.image, id: this.getCognitoId() });
            client.mutate({
                mutation: gql`
                    mutation createUser($id: ID!, $image: String, $name: String!){
                        createUser(input: { id: $id, name: $name, image: $image }) {
                            id
                            image
                        }
                    }
                `,
                variables: {
                    id: this.getCognitoId(),
                    name: user.name,
                    image: user.image
                }
            });
        });
    }
    signOut = () => {
        Auth.signOut().then(data => {
            navigate('/');
            this.setState({ user: '', userSet: false, image: '', id: ''});
        });
    }
    handleFailure = () => null;

    constructor(props) {
        super(props);
        const cacheInfo = Cache.getItem('federatedInfo');

        if (cacheInfo) {
            const { user, expires_at } = cacheInfo;
            const now = new Date().getTime();
            if (expires_at > now) {
                this.state = {
                    user: user.name,
                    id: this.getCognitoId(),
                    userSet: true,
                    image: user.image
                }
            }
        }
    }

    render() {
        return (
            <Context.Provider value={{ signIn: this.signIn, signOut: this.signOut, ...this.state }}>
                <Wrapper>
                    { this.props.children }
                </Wrapper>
            </Context.Provider>
        );
    }

    // We reference the cognito id in the backend for ownership, so we want to reference
    // it here to determine what UI to show in various parts of the app
    getCognitoId() {
        const storageKeys = Object.keys(localStorage);
        for (let i = 0; i < storageKeys.length; i++) {
            if (storageKeys[i].indexOf('aws.cognito.identity-id') >= 0) {
                return localStorage.getItem(storageKeys[i])
            }
        }
    }
}

