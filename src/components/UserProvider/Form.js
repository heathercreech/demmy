import React from 'react';
import PropTypes from 'prop-types';
import { GoogleLogin } from 'react-google-login';


export const Form = ({ onClick, onSuccess, onFailure }) => (
    <div style={{ textAlign: 'center', margin: 'auto', marginTop: '300px' }}>
        <GoogleLogin onSuccess={ onSuccess } onFailure={ onFailure }/>
    </div>
);
Form.propTypes = {
    onSuccess: PropTypes.func.isRequired,
    onFailure: PropTypes.func.isRequired
};