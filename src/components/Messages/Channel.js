import React from 'react';
import styled from 'styled-components';

import posed from 'react-pose';

export const ChannelBox = styled.div`
    position: relative;
    @media (min-width:768px) {
        //grid-column-start: 4;
    }
    @media (max-width:768px) {
        //grid-column-start: 4;
    }
    height: 100%;
    display: grid;
    border-left : 1px solid lightgrey;
    grid-template-columns: auto auto auto;
    grid-template-rows: auto;
`;
export const ChannelTitle = styled.h5`
    grid-column-start: 1;
    grid-column-end: 4;
    margin: 0;
    height: 100%;
    font-size: 16px;
    letter-spacing: .05em;
    display: grid;
    align-items: center;
    text-align: center;
    background: white;
    box-shadow: 0 4px 1px -4px black;
`;
export const ChannelTimestamp = styled.span`
    color: dimgrey;
    font-size: 10px;
    top: 10px;
    right: 10px;
    position: absolute;
`;
export const ChannelMessageBox = styled(posed.li({
    enter: {
        opacity: 1,
    },
    exit: {
        opacity: 0,
    }
}))`
    padding: 20px;
    border-bottom: 1px solid #EDEDED;
    position: relative;
    text-align: center;
    cursor: pointer;
    border-right: 3px solid ${({ success }) => success ? 'mediumseagreen' : 'indianred'};

    &:hover {
        background: whitesmoke;
    }
`;
export const ChannelUser = styled.span`
    color: dimgrey;
    font-size: 12px;
    top: 10px;
    left: 10px;
    position: absolute;
`;
export const ChannelMessagesWrapper = styled(posed.ul({
    enter: {
        delayChildren: 50,
        staggerChildren: 10,
    },
    exit: {
        delay: 300,
    }
}))`
    width: 100%;
    height: 100%;
    padding: 0;
    margin: 0;
    position: absolute;
    overflow-y: auto;
    grid-column-start: 1;
    grid-column-end: 4;
`;
const SuccessFailText = styled.div`
    color: dimgrey;
    font-style: italic;
    font-size: 12px;
    position: absolute;
    bottom: 10px;
    right: 10px;
`;

export const ChannelMessage = ({ children, user, timestamp, success, ...rest }) => (
    <ChannelMessageBox success={ success } { ...rest }>
        <ChannelTimestamp>{ timestamp }</ChannelTimestamp>
        <ChannelUser>{ user }</ChannelUser>
        <SuccessFailText>{ success ? 'Success!' : 'Failure!' }</SuccessFailText>
        { children }
    </ChannelMessageBox>
);
