import React, { Component } from 'react';
import moment from 'moment';
import { PoseGroup } from 'react-pose';

import { GenesysIcon } from 'components/GenesysIcons';
import { ChannelBox, ChannelMessagesWrapper, ChannelMessage } from './Channel';


export class ChannelMessages extends Component {
    state = {
        messages: [],
        mounted: false,
    }

    componentDidMount() {
        setTimeout(() => this.setState({ mounted: true }), 50);
        this.props.subscribeToNewMessages();
    }

    render() {
        let { messages } = this.props;
        messages = messages ? messages : [];
        return (
            <ChannelBox>
                <ChannelMessagesWrapper pose={ this.state.mounted ? 'enter' : 'exit' }>
                    <PoseGroup>
                    { messages.map(({raw, net, dice, user, timestamp, success}) => (
                        <DiceDropdown dice={ dice } raw={ raw } user={ user } success={ success } key={ timestamp } timestamp={ moment(timestamp).format('YYYY-MM-DD H:MM:SS') }>
                            { net.map((res, index) => (
                                <GenesysIcon type={ res } key={`${res}-${index}`} width="30"/>
                            )) }
                        </DiceDropdown>
                    )) }
                    </PoseGroup>
                </ChannelMessagesWrapper>
            </ChannelBox>
        );
    }
}

class DiceDropdown extends Component {
    state = { open: false }
    handleClick = () => this.setState(prevState => ({ open : !prevState.open }))
    render() {
        let { open } = this.state;
        let { dice, timestamp, success, raw, children, ...rest } = this.props;
        return (
            <ChannelMessage success={ success } timestamp={ timestamp } { ...rest } onClick={ this.handleClick }>
                { children }
                { open && (
                    <div style={{ marginTop: '10px' }}>
                        { dice.map((die, index) => <GenesysIcon key={`${die}-${index}-${timestamp}`} type={ die } width="20"/> ) }
                        <div style={{ marginTop: '10px' }}>
                            { raw.map((die, index) => <GenesysIcon key={`${die}-${index}-${timestamp}`} type={ die } width="20"/> ) }
                        </div>
                    </div>
                ) }
            </ChannelMessage>
        );
    }
}
