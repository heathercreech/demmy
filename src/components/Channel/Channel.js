import React, { Component } from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { Match } from '@reach/router'

import { Tabs } from 'components/Tabs';
import { ToolsetTabLayout } from 'components/GameTools/ToolsetTab';

import { CharactersTab } from './CharactersTab';


const query = gql`
    query getGame($id: ID!){
        getGame(id: $id) {
            id
            name
            messages {
                dice
                user
                net
                raw
                success
                timestamp
            }
        }
    }
`;


export class Channel extends Component {
    shouldComponentUpdate(nextProps, nextState) {

        return nextProps.userRolls.length !== this.props.userRolls.length;
    }
    render() {
        let { setDice } = this.props;
        return (
            <Match path='/:game'>
            {({match}) => (match &&
                <Query query={ query } variables={{ id: match.game }}>
                    { ({ subscribeToMore, loading, error, data }) => {
                        return (
                            <ToolsetTabLayout>
                                <Tabs active='characters'>
                                    <CharactersTab setDice={ setDice }/>
                                </Tabs>
                            </ToolsetTabLayout>
                        );
                    } }
                </Query>
            ) }
            </Match>
        );
    }
}

export const ChannelWithRouter = Channel;
