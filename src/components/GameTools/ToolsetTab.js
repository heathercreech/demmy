import React from 'react';

import { TabHeading, TabContent } from 'components/Tabs';
import { TabHeading as StyledTabHeading } from 'components/Tabs/TabHeading';
import { GridTemplate } from 'components/GridTemplate';


export const  ToolsetTabLayout = GridTemplate.extend.attrs({
    template: '40px auto / repeat(5, 1fr)'
})`
`;

export const ToolsetTabHeading = ({ children }) => (
    <TabHeading>
    {({ active, handleClick }) => (
        <StyledTabHeading active={ active } onClick={ handleClick }>{ children }</StyledTabHeading>
    )}
    </TabHeading>
);

export const ToolsetTabContent = ({ children }) => (
    <TabContent>
        <div style={{ borderLeft: '1px solid lightgrey', marginTop: '30px', gridColumn: '1 / 6' }}>{ children }</div>
    </TabContent>
)
