import React, { Component, Fragment } from 'react';
import styled from 'styled-components';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
// import { withStyles } from '@material-ui/core/styles';
//import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import { UserConsumer } from 'components/UserProvider';
import { LoginButton } from 'components/LoginButton';
import { AccountButton } from './AvatarButton';
import { Drawer } from './Drawer';


const Wrapper = styled.div`
    color: white;
`;


export class Header extends Component {
    state = {
        open: false

    }

    handleOpen = () => this.setState({ open: true });
    handleClose = () => this.setState({ open: false });

    render() {
        let { open } = this.state;
        return (
            <Fragment>
                <AppBar
                    style={{
                        height: '7%',
                        justifyContent: 'center',
                        boxShadow: '0px 0px 2px 1px rgba(0, 0, 0, 0.2)'
                    }}
                >
                    <Wrapper>
                        <Toolbar disableGutters={ true }>
                            <IconButton
                                onClick={ this.handleOpen }
                                arial-label='Menu'
                                style={{ float: 'right' }}
                            >
                                <MenuIcon/>
                            </IconButton>
                            <Typography
                                variant='title'
                                style={{ flex: '1' }}
                            >
                                Demmy
                            </Typography>
                            <UserConsumer>
                            {({ userSet, signIn }) => (
                                <div>
                                    { userSet && (
                                        <AccountButton />
                                    ) }
                                    { !userSet && (
                                        <LoginButton
                                            onSuccess={ signIn }
                                            onFailure={ () => null }
                                        >
                                            Login with Google
                                        </LoginButton>
                                    ) }
                                </div>                                
                            )}
                            </UserConsumer>
                        </Toolbar>
                    </Wrapper>
                </AppBar>
                <Drawer
                    open={ open }
                    onClose={ this.handleClose }
                    onOpen={ this.handleOpen }
                />
            </Fragment>
        );
    }
}