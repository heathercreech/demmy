import React, { Component } from 'react';


const Context = React.createContext();
export const TabContentConsumer = Context.Consumer;


export class TabContentProvider extends Component {
    state = { content: '' };
    updateContent = content => this.setState({ content });

    render() {
        return (
            <Context.Provider value={{ content: this.state.content, updateContent: this.updateContent }}>
                { this.props.children }
            </Context.Provider>
        );
    }
}
