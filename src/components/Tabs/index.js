export { Tabs }  from './Tabs';
export { default as TabHeading } from './TabHeading';
export { default as TabContent } from './TabContent';
export { default as Tab } from './Tab';