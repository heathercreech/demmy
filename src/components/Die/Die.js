import React, { Fragment } from 'react'
import styled from 'styled-components'
import noop from 'utils/noop'
import { GenesysIcon } from 'components/GenesysIcons'


const Container = styled.div`
    display: inline-block;
    position: relative;
    padding: 20px 0 15px 0;
`
const Wrapper = styled.div`
    position: relative;
    cursor: pointer;
    display: inline-block;
    width: 50px;
    height: 50px;
`
export const Die = GenesysIcon.extend`height: 100%;`
const CountWrapper = styled.div`
    position: absolute;
    top: 0;
    width: 100%;
    height: 100%;
    display: grid;
    justify-items: center;
    align-content: center;
`
const Count = styled.div`
    font-size: 16px;
    display: grid;
    grid-template: auto / auto;
    justify-items: center;
    align-content: center;
    width: 20px;
    height: 20px;
    font-weight: 600;
    border-radius: 50%;
    color: white;
    background: rgba(0, 0, 0, 0.5);
    text-shadow: 0 0 0 black;
    text-align: center;
    #transition: background-color .2s;

    ${ Wrapper }:hover & {
        background-color: rgba(0, 0, 0, 0.7);
    }
`

const PulseCircle = styled.div`
    display: grid;
    grid-template: auto / auto;
    justify-items: center;
    align-content: center;
    border-radius: 50%;
    @keyframes growshrink {
        from {
            padding: 0;
            background: rgba(0, 0, 0, 0.2);
        }

        to {
            padding:5px;
            background: rgba(0, 0, 0, 0.5);
        }
    }
    ${ Wrapper }:hover &{
        #animation-name: growshrink;
        #animation-duration: .6s;
        #animation-iteration-count: infinite;
        #animation-direction: alternate;
    }
`;


const IncrementButton = styled.button.attrs({children: <Fragment>&#8593;</Fragment>})`
    position: absolute;
    top: 0;
    background: none;
    border: none;
    cursor: pointer;
    width: 50px;
`;

const DecrementButton = styled.button.attrs({children: <Fragment>&#8595;</Fragment>})`
    position: absolute;
    background: none;
    border: none;
    cursor: pointer;
    bottom: 0;
    width: 50px;
`;

export const DieWithCount = ({ color, onClick=noop, increment=noop, decrement=noop, value }) => {
    // const [count, increment, decrement, reset] = useDieCount(value, onChange)

    return (
        <Container>
            <IncrementButton onClick={ increment } aria-label={`Increment ${ color } die count by 1`} />
            <DecrementButton onClick={ decrement } aria-label={`Decrement ${ color } die count by 1`} />
            <Wrapper
                onClick={ onClick }
                onWheel={ e => e.deltaY > 0 ? decrement() : increment() }
            >
                <Die type={ color } />
                <CountWrapper>
                    <Count>
                        <PulseCircle>{ value }</PulseCircle>
                    </Count>
                </CountWrapper>
            </Wrapper>
        </Container>
    )
}
