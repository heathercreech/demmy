import styled from 'styled-components';


export const GridTemplate = styled.div`
    display: grid;
    height: 100%;
    justify-content: ${({ justify }) => justify};
    align-items: ${({ align }) => align};
    grid-template: ${({ template }) => template };

    @media (max-width: 1024px) {
        grid-template: ${({ small }) => small };
    }
    @media (min-width: 1024px) {
        grid-template: ${({ large }) => large };
    }

    grid-gap: ${({ gap }) => gap};
`;
