import React, { Fragment } from 'react';

import { DieWithCount } from 'components/Die';


export const GenesysDiceSet = ({ onChange, ...rest }) => (
    <Fragment>
        {['blue', 'green', 'yellow', 'black', 'purple', 'red'].map(color => (
            <DieWithCount
                value={ rest[color] }
                color={ color }
                key={ color }
                onChange={ value => onChange(color, value) }
            >
            </DieWithCount>
        ))}
    </Fragment>
);
