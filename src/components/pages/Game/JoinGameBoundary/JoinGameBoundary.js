import React, { Fragment } from 'react';
import { Query, Mutation } from 'react-apollo';
import { Match } from '@reach/router';
import Button from '@material-ui/core/Button'

import { UserBoundary } from 'components/UserBoundary';
import { GridTemplate } from 'components/GridTemplate';
import { LoginButton } from 'components/LoginButton';
import { UserConsumer } from 'components/UserProvider';
import { GameInvite, GetGame } from './JoinGameBoundary.gql'


const JoinGrid = GridTemplate.extend.attrs({
    template: 'auto / auto ',
    justify: 'center',
    align: 'center'
})``;


export const JoinGameBoundary = ({ children }) => (
    <UserConsumer>
    {(user) => (
        <UserBoundary
            renderBoundary={() => (
                <JoinGrid>
                    <LoginButton onSuccess={ user.signIn } onFailure={ () => null }>
                        Login to continue
                    </LoginButton>
                </JoinGrid>
            )}
        >
            <Match path='/:game'>
            {({ match }) => match && (
                <Query query={ GetGame } variables={{ id: match.game }}>
                {({ loading, error, data }) => (
                    <Fragment>
                    { !loading && !error && data && data.getGame && (
                        
                                <Fragment>
                                    { (!data.getGame.members.includes(user.id) && data.getGame.creator !== user.id)
                                        ? (
                                            <Mutation
                                                mutation={ GameInvite }
                                                variables={{ id: match.game, user: user.id }}
                                            >
                                            {(gameInvite, { loading, error, data }) => (
                                                <JoinGrid>
                                                    <Button
                                                        onClick={ gameInvite }
                                                        variant='outlined'
                                                        size='large'
                                                    >
                                                        Join game
                                                    </Button>
                                                </JoinGrid>
                                            )}
                                            </Mutation>
                                        )
                                        : children
                                    }
                                    
                                </Fragment>
                        
                    ) }
                    
                    </Fragment>
                ) }
                </Query>
            )}
            </Match>
        </UserBoundary>
    )}
    </UserConsumer>
);