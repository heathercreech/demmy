import React, { Component } from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import { Match } from '@reach/router'

import { ChannelMessages } from 'components/Messages';



const query = gql`
    query getGame($id: ID!){
        getGame(id: $id) {
            id
            name
            messages {
                dice
                user
                net
                raw
                success
                timestamp
            }
        }
    }
`;
const subscription = gql`
    subscription onRollDice {
        onRollDice {
            dice
            user
            net
            success
            timestamp
        }
    }
`;


export class RollLog extends Component {
    shouldComponentUpdate(nextProps, nextState) {

        return nextProps.userRolls.length !== this.props.userRolls.length;
    }
    render() {
        let { userRolls } = this.props;
        userRolls = userRolls ? userRolls : [];
        return (
            <Match path='/:game'>
            {({match}) => (match &&
                <Query query={ query } variables={{ id: match.game }}>
                    { ({ subscribeToMore, loading, error, data }) => {
                        let messages = data && data.getGame ? [...data.getGame.messages] : [];
                        messages = messages.filter(message => userRolls.map(roll => roll.timestamp).indexOf(message.timestamp) === -1);
                        messages = [...userRolls, ...messages];
                        messages = messages.sort((b, a) => a.timestamp < b.timestamp ? -1 : a.timestamp > b.timestamp ? 1 : 0);
                        return !loading && !error && (
                            <ChannelMessages
                                channelName={ data.getGame.name }
                                messages={ messages }
                                subscribeToNewMessages={ () => subscribeToMore({
                                    document: subscription,
                                    variables: {},
                                    updateQuery: (prev, { subscriptionData }) => {
                                        if (!subscriptionData.data) return prev;
                                        const newMessage = subscriptionData.data.onRollDice;
                                        return {
                                            getGame: {
                                                ...prev.getGame,
                                                messages: [newMessage, ...prev.getGame.messages]
                                            }
                                        };
                                    }
                                }) }
                            />
                        );
                    } }
                </Query>
            ) }
            </Match>
        );
    }
}
