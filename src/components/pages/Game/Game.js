import React, { Component } from 'react';

import { ChannelWithRouter as Channel } from 'components/Channel';
import { RollLog } from './RollLog';
import { DicePoolLayout } from './DicePool/DicePoolLayout';
import { DieWithCount } from 'components/Die';
import { RollButton, RollButtonWithMutation } from './DicePool/RollButton';
import { GameLayout } from './GameLayout';
import { JoinGameBoundary } from './JoinGameBoundary';


export class Game extends Component {
    initialDice = {
        blue: 0, green: 0, yellow: 0,
        black: 0, purple: 0, red: 0
    }

    state = {
        dice: { ...this.initialDice },
        count: 1,
        isDragging: false,
        userRolls: []
    }

    setDice = (dice) => this.setState({ dice: { ...this.initialDice, ...dice }})
    handleClick = () => this.setState({ dice: { ...this.initialDice } })
    handleChange = (color, value) => this.setState({ dice: { ...this.state.dice, [color]: value < 0 ? 0 : value > 20 ? 20 : value }})
    handleMutationCompleted = data => this.setState({ userRolls: [ data.rollDice, ...this.state.userRolls ] })

    render() {
        let { dice } = this.state;
        let diceArray = this.buildDiceArray();

        return (
            <JoinGameBoundary path="/:gameId">
                <GameLayout>
                    <Channel setDice={ this.setDice } userRolls={ this.state.userRolls }/>
                    <div style={{ display: 'grid', height: '100%', gridTemplate: '115px auto / auto' }}>
                        <DicePoolLayout>
                            {['blue', 'green', 'yellow', 'black', 'purple', 'red'].map(color => (
                                <DieWithCount
                                    value={ dice[color] }
                                    color={ color }
                                    key={ color }
                                    increment={ () => this.handleChange(color, dice[color] + 1) }
                                    decrement={ () => this.handleChange(color, dice[color] - 1) }
                                    onClick={ this.handleClick }
                                />
                            ))}
                            <RollButtonWithMutation
                                onCompleted={ this.handleMutationCompleted }
                                onClick={ this.handleClick }
                                dice={ diceArray }
                                disabled={ diceArray.length === 0 }
                            />
                            <RollButton onClick={ this.handleClick }>Reset</RollButton>
                        </DicePoolLayout>
                        <RollLog userRolls={ this.state.userRolls }/>
                    </div>
                </GameLayout>
            </JoinGameBoundary>
        );
    }

    buildDiceArray() {
        let { dice } = this.state;
        return Object.keys(dice).length ? Object.keys(dice).map(color => {
            let count = dice[color];
            return Array(count).fill(color, 0);
        }).reduce((a, b) => a.concat(b)) : [];
    }
}
