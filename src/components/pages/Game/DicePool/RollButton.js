import React from 'react';
import styled from 'styled-components';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { Match } from '@reach/router';

import { UserConsumer } from 'components/UserProvider';


const rollDice = gql`
mutation rollDice($dice: [String]!, $user: String, $game: ID!){
    rollDice(input: {user: $user, dice: $dice, game: $game}) {
        raw
        net
        timestamp
        user
        success
        dice
    }
}
`;


export const RollButton = styled.button.attrs({ children: 'Roll'})`
    border: 1px solid ${({ disabled }) => disabled ? 'darkslategrey' : 'darkgrey'};
    color: black;
    background: ${({ disabled }) => disabled ? 'lightgrey' : 'whitesmoke' };
    padding: 15px;
    font-size: 18px;
    font-weight: 600;
    height: 100%;
    width: 100%;

    cursor: ${({ disabled }) => disabled ? 'not-allowed' : 'pointer'};
    &:hover {
        ${({ disabled }) => !disabled ? 'background: ghostwhite;' : ''} 
    }
`;

export const RollButtonWithMutation = ({ dice, disabled, onClick, variables, ...rest }) => (
    <Match path='/:game'>
    {({ match }) => (match && (
        <Mutation mutation={ rollDice } { ...rest }>
        { (rollDice, { data }) => (
            <UserConsumer>
            { ({ user }) => (
                <div>
                    <RollButton
                        onClick={() => {
                            rollDice({ variables: { dice: dice, user: user, game: match.game }}); onClick();
                        }}
                        disabled={ disabled }
                    />
                </div>
            ) }
            </UserConsumer>
        ) }
        </Mutation>
    )) }
    </Match>
);
