import React from 'react';
import { GridTemplate } from 'components/GridTemplate';


const GamesGridTemplate = GridTemplate.extend`
    justify-content: center;
`;
export const GamesPageLayout = (props) => <GamesGridTemplate
    template='max-content max-content / 90%'
    //columns='auto'
    //rows='max-content max-content'
    { ...props }
/>
