import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';


export const EditOption = (props) => (
    <ListItem button { ...props }>
        <ListItemText primary='Edit' />
    </ListItem>
);