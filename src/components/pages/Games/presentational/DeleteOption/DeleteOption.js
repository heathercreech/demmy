import React from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import Typography from '@material-ui/core/Typography';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import { GamesQuery } from '../../Games.gql';


export const DeleteOption = ({ gameId, ...rest }) => (
    <Mutation
        mutation={gql`mutation deleteGame($id: ID!){deleteGame(input: {id: $id}) {id name creator}} `}
        variables={{ id: gameId }}
        update={(store, { data: { deleteGame }}) => {
            const cachedData = store.readQuery({ query: GamesQuery });

            for (let i = 0; i < cachedData.listGames.items.length; i++) {
                let cachedGame = cachedData.listGames.items[i];
                if (cachedGame.id === deleteGame.id) {
                    cachedData.listGames.items.splice(i, 1);
                    break;
                }
            }
            store.writeQuery({query: GamesQuery, data: cachedData })
        }}
    >
    { (deleteGame, { data }) => (!data &&
        <ListItem button onClick={ () => deleteGame()} { ...rest }>
            <Typography variant='subheading' color='error'>
                <ListItemText primary='Delete' disableTypography/>
            </Typography>
        </ListItem>
    ) }
    </Mutation>
);