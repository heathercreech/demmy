import styled from 'styled-components';


export const GamesHeading = styled.h1`
    padding-top: 20px;
    font-weight: 500;
    font-family: "Roboto", "Helvetica", "Arial", sans-serif;
    margin: 0
`
