import React, { Component, Fragment } from 'react';
import Modal from '@material-ui/core/Modal';

import { AddButton } from '../presentational/AddButton';


export class AddButtonWithModal extends Component {
    state = { open: false }
    
    handleClick = () => this.setState({ open: true })
    handleClose = () => this.setState({ open: false })

    render() {
        let { open } = this.state;
        return (
            <Fragment>
                <AddButton onClick={ this.handleClick }/>
                <Modal
                    aria-labelledby='create-game-dialog-title'
                    aria-describedby='create-game-dialog-description'
                    open={ open }
                    onClose={ this.handleClose }
                >
                    <div>Hello World</div>
                </Modal>
            </Fragment>
        );
    }
}
