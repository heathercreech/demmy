import React, { Component, Fragment } from 'react';
import IconButton from '@material-ui/core/IconButton';
import MoreHoriz from '@material-ui/icons/MoreHoriz';
import Popover from '@material-ui/core/Popover';
import List from '@material-ui/core/List';

import { UserConsumer } from 'components/UserProvider';
import { InviteOption } from './presentational/InviteOption';
import { EditOption } from './presentational/EditOption';
import { DeleteOption } from './presentational/DeleteOption';


export class GameOptionButton extends Component {
    state = { open: false, el: null }

    handleClick = e => this.setState({ open: true, el: e.currentTarget })
    handleClose = () => this.setState({ open: false, el: null })

    render() {
        let { open, el } = this.state;
        let { gameId, gameCreator, onDelete } = this.props;
        
        return (
            <Fragment>
                <IconButton onClick={ this.handleClick } style={{ float: 'right' }}>
                    <MoreHoriz />
                </IconButton>
                <Popover
                    open={ open }
                    onClose={ this.handleClose }
                    anchorEl={ el }
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'center'}}
                    transformOrigin={{ vertical: 'top', horizontal: 'center'}}
                >
                    <List>
                        <EditOption/>
                        
                        <UserConsumer>
                        {({ id }) => (
                            gameCreator === id && (
                                <Fragment>
                                    <InviteOption gameId={ gameId } onClick={ this.handleClose }/>
                                    <DeleteOption gameId={ gameId } onDelete={ onDelete }/>
                                </Fragment>
                            )
                        ) }
                        </UserConsumer>
                    </List>
                </Popover>
            </Fragment>
        )
    }

}