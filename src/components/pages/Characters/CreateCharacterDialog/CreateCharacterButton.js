import React from 'react';
import { Mutation } from 'react-apollo';
import Button from '@material-ui/core/Button';

import { CharactersQuery } from 'components/pages/Characters/CharacterTable/Characters.gql'
import { CreateCharacterMutation } from './CreateCharacterButton.gql';


export const CreateCharacterButton = ({ variables, onClick }) => (
    <Mutation
        mutation={ CreateCharacterMutation }
        variables={ variables }
        update={(store, { data: { createCharacter } }) => {
            const cachedData = store.readQuery({ query: CharactersQuery });
            cachedData.listCharacters.items.push(createCharacter)
            store.writeQuery({query: CharactersQuery, data: cachedData })
        }}
    >
    {(createCharacter, { loading, error, data }) => (
        <Button
            onClick={() => {
                onClick();
                createCharacter();
            }}
        >
            Create!
        </Button>
    )}
    </Mutation>
);