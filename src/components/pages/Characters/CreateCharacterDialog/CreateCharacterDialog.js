import React, { Component, Fragment } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

import { Form } from './Form';
import { CreateCharacterButton } from './CreateCharacterButton';


export class CreateCharacterDialog extends Component {

    state = {
        dialogOpen: false,
        name: null,
        characteristics: {}
    }

    handleOpen = () => this.setState({ dialogOpen: true })
    handleClose = () => this.setState({ dialogOpen: false })

    handleNameChange = e => this.setState({ name: e.target.value })
    handleCharacteristicChange = e => this.setState({
        characteristics: {
            ...this.state.characteristics,
            [e.target.id]: e.target.value
        }
    })

    render() {
        return (
            <Fragment>
                <Button onClick={ this.handleOpen }>Create</Button>
                <Dialog
                    open={ this.state.dialogOpen }
                    onClose={ this.handleClose }
                    aria-labelledby='create-character-dialog-title'
                >
                    <DialogTitle id='create-character-dialog-title'>
                        Create Your Character
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Fill out the form below to create a new character.
                        </DialogContentText>
                        <Form
                            onNameChange={ this.handleNameChange }
                            onCharacteristicChange={ this.handleCharacteristicChange }
                        />
                        <DialogActions>
                            <Button onClick={ this.handleClose }>Cancel</Button>
                            <CreateCharacterButton
                                variables={{
                                    name: this.state.name,
                                    characteristics: this.state.characteristics,
                                    games: [],
                                    skills: [
                                        { characteristic: 'intellect', name: "Alchemy", rank: 0 },
                                        { characteristic: 'intellect', name: "Astrocartography", rank: 0 },
                                        { characteristic: 'brawn', name: "Athletics", rank: 0 },
                                        { characteristic: 'brawn', name: "Brawl", rank: 0 },
                                        { characteristic: 'presence', name: "Charm", rank: 0 },
                                        { characteristic: 'willpower', name: "Coercion", rank: 0 },
                                        { characteristic: 'intellect', name: "Computers", rank: 0 },
                                        { characteristic: 'presence', name: "Cool", rank: 0 },
                                        { characteristic: 'agility', name: "Coordination", rank: 0 },
                                        { characteristic: 'cunning', name: "Deception", rank: 0 },
                                        { characteristic: 'willpower', name: "Discipline", rank: 0 },
                                        { characteristic: 'agility', name: "Driving", rank: 0 },
                                        { characteristic: 'agility', name: "Gunnery", rank: 0 },
                                        { characteristic: 'presence', name: "Leadership", rank: 0 },
                                        { characteristic: 'intellect', name: "Mechanics", rank: 0 },
                                        { characteristic: 'intellect', name: "Medicine", rank: 0 },
                                        { characteristic: 'brawn', name: "Melee", rank: 0 },
                                        { characteristic: 'presence', name: "Negotiation", rank: 0 },
                                        { characteristic: 'intellect', name: "Operating", rank: 0 },
                                        { characteristic: 'cunning', name: "Perception", rank: 0 },
                                        { characteristic: 'agility', name: "Piloting", rank: 0 },
                                        { characteristic: 'agility', name: "Ranged", rank: 0 },
                                        { characteristic: 'brawn', name: "Resilience", rank: 0 },
                                        { characteristic: 'agility', name: "Riding", rank: 0 },
                                        { characteristic: 'cunning', name: "Skulduggery", rank: 0 },
                                        { characteristic: 'agility', name: "Stealth", rank: 0 },
                                        { characteristic: 'cunning', name: "Streetwise", rank: 0 },
                                        { characteristic: 'cunning', name: "Survival", rank: 0 },
                                        { characteristic: 'willpower', name: "Vigilance", rank: 0 },
                                    ]
                                }}
                                onClick={ this.handleClose }
                            />
                        </DialogActions>
                    </DialogContent>
                </Dialog>
            </Fragment>
        );
    }
}
