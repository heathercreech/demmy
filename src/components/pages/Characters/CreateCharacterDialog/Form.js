import React, { Fragment } from 'react';
import TextField from '@material-ui/core/TextField';

const Input = (props) => (
    <TextField
        autoComplete='off'
        margin='dense'
        fullWidth
        { ...props }
    />
)

export const Form = ({ onNameChange, onCharacteristicChange }) => (
    <Fragment>
        <Input
            autoFocus
            id="name"
            label="Name"
            onChange={ onNameChange }
        />
        <Input
            id="brawn"
            label="Brawn"
            onChange={ onCharacteristicChange }
        />
        <Input
            id="agility"
            label="Agility"
            onChange={ onCharacteristicChange }
        />
        <Input
            id="intellect"
            label="Intellect"
            onChange={ onCharacteristicChange }
        />
        <Input
            id="cunning"
            label="Cunning"
            onChange={ onCharacteristicChange }
        />
        <Input
            id="willpower"
            label="Willpower"
            onChange={ onCharacteristicChange }
        />
        <Input
            id="presence"
            label="Presence"
            onChange={ onCharacteristicChange }
        />
    </Fragment>
);