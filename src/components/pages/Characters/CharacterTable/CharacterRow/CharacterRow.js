import React from 'react';

import { CharactersQuery } from '../Characters.gql';
import { CharacterDialog } from 'components/CharacterDialog';
import { Row } from '../Table';


export const CharacterRow = ({ children, character }) => (
    <CharacterDialog
        character={ character }
        render={({ open }) => (
            <Row onClick={ open }>
                { children }
            </Row>
        )}
        updateStore={(store, { data: { updateCharacter } }) => {
            const cachedData = store.readQuery({ query: CharactersQuery });
            cachedData.listCharacters.items.map(
                character => (
                    character.id === updateCharacter.id
                        ? { ...character, ...updateCharacter }
                        : character
                )
            );
            store.writeQuery({query: CharactersQuery, data: cachedData })
        }}
    />
);
