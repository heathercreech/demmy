import styled from 'styled-components';


export const TableHeading = styled.p`
    margin: 0;
    padding: 10px;
    font-weight: 600;
    color: #6b6b6b;
    text-align: ${props => props.align};
`;
