import React from 'react';
import styled from 'styled-components';

import { GridTemplate } from 'components/GridTemplate';
import { Link } from 'components/Link';
import { colors } from 'theme'


const Heading = styled.h1`
    font-size: 3rem;
`;
const Box = styled(Link)`
    display: inline-block;
    padding: 40px 80px;
    border: 1px solid black;
    border-radius: 5px;
    color: black;
    font-size: 2rem;
    letter-spacing: 2px;

    &:hover {
        border: 1px solid ${ colors.accent };
    }
`;

const Paragraph = styled.p`
    font-size: 1.2rem;
    margin-bottom: 80px;
`;


export const Home = (props) => (
    <GridTemplate justify='center' template='auto / 95%'>
        <div style={{ textAlign: 'center' }}>
            <Heading>
                Demmy
            </Heading>

            <Paragraph>A tool for playing games in the Genesys role-playing system.</Paragraph>

            <GridTemplate gap='40px' small='repeat(3, fit-content(100%)) / auto' template='repeat(3, fit-content(100%)) / auto auto auto auto'>
                <Box to='/games'>
                    Games
                </Box>
                <Box to='/characters'>
                    Characters
                </Box>
            </GridTemplate>
        </div>
    </GridTemplate>
);
