import React, { Component } from 'react';
import { navigate } from '@reach/router';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import FormGroup from '@material-ui/core/FormGroup';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { CreateGameLayout } from './presentational/CreateGameLayout';
import { GamesQuery } from 'components/pages/Games/Games.gql';



const mutation = gql`
    mutation createGame($name: String!) {
        createGame(input: { name: $name, members: []}) {
            id
            name
            creator
        }
    }
`;

export const CreateButton = ({ name }) => (
    <Mutation
        mutation={ mutation }
        onCompleted={() => navigate('/games')}
        onError={() => console.log('Error creating game!')}
        update={(store, { data: { createGame } }) => {
            const cachedData = store.readQuery({ query: GamesQuery });
            cachedData.listGames.items.push(createGame);
            store.writeQuery({query: GamesQuery, data: cachedData })

        }}
        variables={{ name }}
    >
    { (createGame, { data }) => (
        <Button
            onClick={() => { createGame() }}
            color='secondary'
            size='small'
        >
            create { data && 'done' }
        </Button>
    ) }
    </Mutation>
)



export class CreateGame extends Component {
    state = {
        name: ''
    }

    handleChange = e => this.setState({ name: e.target.value })

    render() {
        return (
            <CreateGameLayout>
                <form onSubmit={e => e.preventDefault()}>
                <FormGroup styled={{ gridColumn: '2' }}>
                    <TextField label='Name' onChange={ this.handleChange }/>
                    <CreateButton name={ this.state.name }/>
                </FormGroup>
                </form>
            </CreateGameLayout>
        )
    }
}
