import React from 'react';
import { GridTemplate } from 'components/GridTemplate';


export const CreateGameLayout = (props) => <GridTemplate
    justify='center'
    align='center'
    { ...props }
/>
