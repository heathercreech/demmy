import React from 'react';
import { GridTemplate } from 'components/GridTemplate';


const FormGridTemplate = GridTemplate.extend`
    width: 100px;
    height: 150px;
    margin: auto;
    grid-column: 2;
`;
export const FormLayout = (props) => <FormGridTemplate template='40px 40px 30px 40px / auto' { ...props } />
