import React, { Component } from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';


const Context = React.createContext();
export const NotificationConsumer = Context.Consumer;


export class NotificationProvider extends Component {
    state = { open: false, autoHideDuration: 2000 }

    handleOpen = (content, autoHideDuration) => this.setState({
        open: true,
        content,
        autoHideDuration: autoHideDuration ? autoHideDuration : this.state.autoHideDuration
    })
    handleClose = () => this.setState({ open: false, content: '' })

    render() {
        return (
            <Context.Provider
                value={{
                    notify: this.handleOpen,
                    close: this.handleClose,
                    ...this.state
                }}
            >
                { this.props.children }
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    open={this.state.open}
                    autoHideDuration={ this.state.autoHideDuration }
                    onClose={this.handleClose}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id">{ this.state.content }</span>}
                    action={[
                        <IconButton
                            key="close"
                            aria-label="Close"
                            color="inherit"
                            onClick={this.handleClose}
                        >
                            <CloseIcon />
                        </IconButton>
                    ]}
                />
            </Context.Provider>
        )
    }
}