import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';
import { checkA11y } from '@storybook/addon-a11y';
import { RangeMap } from '.';
import data from './data.json'

storiesOf('RangeMap', module)
    .addDecorator(checkA11y)
    .add('RangeMap', () => (
        <Fragment>
            <RangeMap data={ data } />
        </Fragment>
    ))