import { GridTemplate } from 'components/GridTemplate';


export const CharacteristicsLayout = GridTemplate.extend.attrs({
    template: 'auto / 1fr 1fr 1fr'
})`
    grid-gap: 20px;
`;