import React, { Fragment } from 'react';
import styled from 'styled-components';
import TextField from '@material-ui/core/TextField';

import { FormConsumer } from '../FormProvider'


const CharacteristicFieldLabel = styled.h4`
    margin: 0;
    font-size: 14px;
    font-weight: 400;
    color: darkgrey;
`;


const CharacteristicField = ({ label, value, ...rest }) => (
    <FormConsumer>
    {({ handleCharacteristicChange, editing }) => (
        <Fragment>
            { editing && (
                <TextField onChange={ handleCharacteristicChange } label={ label } value={ value } { ...rest }/>
            ) }
            { !editing && (
                <div>
                    <CharacteristicFieldLabel>{ label }</CharacteristicFieldLabel>
                    <div style={{ padding: '10px 0'}}>{ value }</div>
                </div>
            ) }
        </Fragment>
    )}
    </FormConsumer>
);

export const CharacteristicFields = () => (
    <FormConsumer>
    {({ editing, characteristics: { brawn, agility, intellect, cunning, willpower, presence }}) => (
        <Fragment>
            <CharacteristicField
                value={ brawn }
                id='brawn'
                label={ 'Brawn' }
            />
            <CharacteristicField
                value={ agility }
                id='agility'
                label={ 'Agility' }
            />
            <CharacteristicField
                value={ intellect }
                id='intellect'
                label={ 'Intellect' }
            />
            <CharacteristicField
                value={ cunning }
                id='cunning'
                label={ 'Cunning' }
            />
            <CharacteristicField
                value={ willpower }
                id='willpower'
                label={ 'Willpower' }
            />
            <CharacteristicField
                value={ presence }
                id='presence'
                label={ 'Presence' }
            />
        </Fragment>
    )}
    </FormConsumer>
);