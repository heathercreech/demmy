import React, { Component, Fragment } from 'react';
import { Mutation } from 'react-apollo';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { Value } from 'slate';

import { Form } from './Form';
import { UpdateCharacterMutation } from './CharacterDialog.gql';
import { FormProvider } from './FormProvider/FormProvider';


const Editable = ({ editing, value, children }) => (
    <Fragment>
        { !editing && value }
        { editing && children }
    </Fragment>
)

const EditableField = ({ editing, value, ...rest }) => (
    <Editable editing={ editing } value={ value }>
        <TextField
            value={ value }
            { ...rest }
        />
    </Editable>
);

export class CharacterDialog extends Component {

    parse = json => {
        try {
            return Value.fromJSON(JSON.parse(json));
        } catch(e) {
            return Value.fromJSON({
                document: {
                    nodes: [
                        {
                            object: 'block',
                            type: 'paragraph',
                            nodes: [
                                {
                                    object: 'text',
                                    leaves: [
                                        { text: 'Hello World' }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            })
        }
    }

    state = {
        open: false,
        editing: false,
        name: this.props.character.name,
        bio: this.parse(this.props.character.bio),
        npc: this.props.character.npc === undefined ? false : this.props.character.npc,
        skills: this.props.character.skills.map(skill => { let obj = { ...skill }; delete obj['__typename']; return obj}),
        characteristics: {
            brawn: this.props.character.characteristics.brawn,
            agility: this.props.character.characteristics.agility,
            intellect: this.props.character.characteristics.intellect,
            cunning: this.props.character.characteristics.cunning,
            willpower: this.props.character.characteristics.willpower,
            presence: this.props.character.characteristics.presence,
        },
        strain: { current: this.props.character.strain.current, threshold: this.props.character.strain.threshold },
        games: this.props.character.games,
        changed: false
    }


    handleOpen = () => this.setState({ open: true })
    handleClose = () => this.setState({ open: false, editing: false })

    handleNPCChange = () => this.setState(prevState => ({ npc: !prevState.npc, changed: true }))
    handleFieldChange = e => this.setState({ [e.target.id]: e.target.value, changed: true })
    handleBioChange = ({ value }) => {
        this.setState({ bio: value, changed: true });
        console.log('Called bio change');
    }
    handleCharacteristicChange = e => this.setState({ characteristics: { ...this.state.characteristics, [e.target.id]: e.target.value }, changed: true });
    handleStrainChange = e => this.setState({ strain: { ...this.state.strain, current: parseInt(e.target.value, 10) || 0 }, changed: true })
    handleStrainThresholdChange = e => this.setState({ strain: { ...this.state.strain, threshold: parseInt(e.target.value, 10) || 0 }, changed: true })
    handleSkillChange = skill => {
        let skills = [ ...this.state.skills ];
        let found = false;
        for (let i = 0; i < skills.length; i++) {
            let currentSkill = skills[i];
            if (currentSkill.name === skill.name) {
                skills[i] = skill;
                found = true;
            }
        }
        if (!found) {
            skills.push(skill);
        }
        this.setState({ skills, changed: true })
    }
    handleGameChange = e => {
        let games = [ ...this.state.games ];
        let index = games.indexOf(e.target.name);
        if (index !== -1) {
            games.splice(index, 1);
        } else {
            games.push(e.target.name);
        }
        this.setState({ games, changed: true })
    }

    render() {
        let { render, character, updateStore } = this.props;
        let { open, editing, name, bio, npc, strain, skills, characteristics, games, changed } = this.state;
        return (
            <Fragment>
                { render({ open: this.handleOpen, close: this.handleClose }) }
                <Dialog
                    open={ open }
                    onClose={ this.handleClose }
                    fullWidth={ true }
                    aria-labelledby={`${ character.id }-dialog-title`}
                >
                    <DialogTitle id={`${ character.id }-dialog-title`}>
                        <div style={{ float: 'right' }}>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={ this.state.npc }
                                        onChange={ this.handleNPCChange }
                                        value="npc"
                                        disabled={ !editing }
                                    />
                                }
                                label='NPC'
                            />
                        </div>
                        <EditableField
                            id='name'
                            label='Name'
                            editing={ editing }
                            value={ name }
                            onChange={ this.handleFieldChange }
                        />
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText></DialogContentText>
                        <FormProvider
                            handleFieldChange={ this.handleFieldChange }
                            handleCharacteristicChange={ this.handleCharacteristicChange }
                            handleSkillChange={ this.handleSkillChange }
                            handleStrainChange={ this.handleStrainChange }
                            handleStrainThresholdChange={ this.handleStrainThresholdChange }
                            handleGameChange={ this.handleGameChange }
                            handleBioChange={ this.handleBioChange }
                            onSkillClick={ this.props.onSkillClick }
                            { ...this.state }
                        >
                            <Form />
                        </FormProvider>
                        <DialogActions>
                            <Button onClick={ this.handleClose }>Close</Button>
                            { !editing && (
                                <Button onClick={() => this.setState({editing: true })}>Edit</Button>
                            ) }
                            { editing && (
                                <Mutation
                                    mutation={ UpdateCharacterMutation }
                                    variables={{
                                        id: character.id,
                                        name,
                                        bio: JSON.stringify(bio.toJSON()),
                                        npc,
                                        skills,
                                        strain,
                                        characteristics,
                                        games
                                    }}
                                    update={ updateStore }
                                >
                                {(updateCharacter) => (
                                    <Button
                                        onClick={() => {
                                            if (changed) {
                                                updateCharacter();
                                            }
                                            this.setState({ editing: false, changed: false });
                                        }}
                                    >
                                        Save
                                    </Button>
                                )}
                                </Mutation>
                            ) }
                        </DialogActions>
                    </DialogContent>
                </Dialog>
            </Fragment>
        );
    }
}
