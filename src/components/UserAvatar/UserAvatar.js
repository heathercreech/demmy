import React, { Fragment } from 'react';
import { Query } from 'react-apollo';

import { UserQuery } from './UserQuery.gql';
import { Avatar } from 'components/pages/Games/presentational/Avatar';


export const UserAvatar = ({ id }) => (
    <Query query={ UserQuery } variables={{ id }}>
    {({ loading, error, data }) => (
        <Fragment>
            {!loading && !error && data.getUser && (
                <Avatar
                    alt={ data.getUser.name }
                    title={ data.getUser.name }
                    src={ data.getUser.image }
                />
            )}
        </Fragment>
    )}
    </Query>
);