import React from 'react';
import styled from 'styled-components';

import threat from './assets/threat.svg';
import failure from './assets/failure.svg';
import despair from './assets/despair.svg';
import black from './assets/black.svg';
import purple from './assets/purple.svg';
import red from './assets/red.svg';
import advantage from './assets/advantage.svg';
import success from './assets/success.svg';
import triumph from './assets/triumph.svg';
import blue from './assets/blue.svg';
import green from './assets/green.svg';
import yellow from './assets/yellow.svg';

const iconTypes = {
    threat, failure, despair, black, purple, red, advantage, success, triumph, blue, green, yellow
}

export const GenesysIcon = styled.img.attrs({
    src: ({ type }) => iconTypes[type],
    alt: ({ type }) => `Genesys Icon - ${ type }`
})``;
export const GenesysThreat = (props) => <img src={ threat } alt='Genesys Icon - Threat'  { ...props } />
export const GenesysFailure = (props) => <img src={ failure } alt='Genesys Icon - Failure'  { ...props } />
export const GenesysDespair = (props) => <img src={ despair } alt='Genesys Icon - Despair'  { ...props } />
export const GenesysBlack = (props) => <img src={ black } alt='Genesys Icon - Black'  { ...props } />
export const GenesysPurple = (props) => <img src={ purple } alt='Genesys Icon - Purple'  { ...props } />
export const GenesysRed = (props) => <img src={ red } alt='Genesys Icon - Red'  { ...props } />
export const GenesysAdvantage = (props) => <img src={ advantage } alt='Genesys Icon - Advantage'  { ...props } />
export const GenesysSuccess = (props) => <img src={ success } alt='Genesys Icon - Success'  { ...props } />
export const GenesysTriumph = (props) => <img src={ triumph } alt='Genesys Icon - Triumph'  { ...props } />
export const GenesysBlue = (props) => <img src={ blue } alt='Genesys Icon - Blue'  { ...props } />
export const GenesysGreen = (props) => <img src={ green } alt='Genesys Icon - Green'  { ...props } />
export const GenesysYellow = (props) => <img src={ yellow } alt='Genesys Icon - Yellow'  { ...props } />
