import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';
import { checkA11y } from '@storybook/addon-a11y';
import { GenesysIcon } from '.';


storiesOf('GenesysIcon', module)
    .addDecorator(checkA11y)
    .add('Genesys dice', () => (
        <Fragment>
            <GenesysIcon type='black' />
            <GenesysIcon type='purple' />
            <GenesysIcon type='red' />
            <GenesysIcon type='blue' />
            <GenesysIcon type='green' />
            <GenesysIcon type='yellow' />
        </Fragment>
    ))
    .add('Genesys results', () => (
        <Fragment>
            <GenesysIcon type='threat' />
            <GenesysIcon type='failure' />
            <GenesysIcon type='despair' />
            <GenesysIcon type='advantage' />
            <GenesysIcon type='success' />
            <GenesysIcon type='triumph' />
        </Fragment>
    ))