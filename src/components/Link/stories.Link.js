import React from 'react';
import { storiesOf } from '@storybook/react';
import { Link } from './Link';


storiesOf('Link', module)
    .add('Link', () => (
        <Link to='/'>This is a link</Link>
    ))