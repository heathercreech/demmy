import { useState } from 'React';


function useNumberWithBounds(min, max, defaultValue=0) {
    const [number, setNumber] = useState(defaultValue)
    const setNumberBetweenMinMax = (updatedValue) => {
        let success = false
        if (updatedValue >= max) {
            success = number !== max
            setNumber(max)
        } else if (updatedValue <= min) {
            success = number !== min
            setNumber(min)
        } else {
            success = number !== updatedValue
            setNumber(updatedValue)
        }
        return success
    }
    return [number, setNumberBetweenMinMax]
}
