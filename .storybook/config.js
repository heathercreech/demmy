import { configure } from '@storybook/react';
import { setOptions } from '@storybook/addon-options';


setOptions({
    sortStoriesByKind: true
});


function requireAll(requireContext) {
    return requireContext.keys().map(requireContext);
}

function loadStories() {
    requireAll(require.context("../src", true, /stories\..*\.js$/));
}

configure(loadStories, module);
